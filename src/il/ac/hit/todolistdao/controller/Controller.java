package il.ac.hit.todolistdao.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import il.ac.hit.todolistdao.model.*;

/**
 * Servlet implementation class Login.
 */
public class Controller extends HttpServlet
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new controller.
	 *
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller()
	{
		super();
	}

	/**
	 * Do get.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// http://localhost:8080/ToDoListDAO/Controller/*
		String path = request.getPathInfo();
		RequestDispatcher dispatcher = null;
		User newUser = null;
		boolean alphanumeric = true;
		IToDoListDAO toDoListDAO = ToDoListDAO.getInstance();

		switch (path)
		{
		// Register new user
		case "/register":
		{
			try
			{
				String userName = request.getParameter("userName");
				String password = request.getParameter("password");
				String isAdmin = request.getParameter("admin");
				// check if the username and password strings is alphanumeric
				alphanumeric = (isAlphaNumeric(userName) && isAlphaNumeric(password));

				// if the user didn't enter username or password, the user gets
				// a message
				if (userName.equals("") || password.equals(""))
				{
					request.setAttribute("userMessage", "Username or password can't be empty");
					dispatcher = getServletContext().getRequestDispatcher("/register.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					// if username&password are alphanumeric, new user will be
					// created
					if (alphanumeric)
					{
						newUser = new User(userName, password);

						// test register succeeded
						if (toDoListDAO.register(newUser))
						{
							Cookie cookie = new Cookie("userID", String.valueOf(newUser.getId()));
							if (cookie != null)
							{
								cookie.setMaxAge(3600);
								response.addCookie(cookie);
							}

							request.getSession().setAttribute("userID", newUser.getId());
							request.getSession().setAttribute("userName", newUser.getName());
							if (isAdmin.equals("true"))
							{
								toDoListDAO.setAdmin(newUser);
								request.getSession().setAttribute("admin", true);
							}
							dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
							dispatcher.forward(request, response);
						}
						else
						{
							request.setAttribute("userMessage", "User already exist");
							dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
							dispatcher.forward(request, response);
						}
					}
					else
					{
						// if username or password arn't alphanumeric, message
						// will be shown
						request.setAttribute("userMessage", "Username must contains only letters or numbers");
						dispatcher = getServletContext().getRequestDispatcher("/register.jsp");
						dispatcher.forward(request, response);
					}
				}
			}
			catch (ToDoListDAOException e)
			{
				request.setAttribute("userMessage", e.getMessage());
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Connect existing user
		case "/login":
		{
			try
			{
				String userName = request.getParameter("userName");
				String password = request.getParameter("password");

				alphanumeric = (isAlphaNumeric(userName) && isAlphaNumeric(password));

				// if the user didn't enter username or password, the user gets
				// a message
				if (userName.equals("") || password.equals(""))
				{
					request.setAttribute("userMessage", "Username or password can't be empty");
					dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);
				}
				if (alphanumeric)
				{
					newUser = new User(userName, password);
					// test if user exists
					if (toDoListDAO.connect(newUser))
					{
						Cookie cookie = new Cookie("userID", String.valueOf(newUser.getId()));
						if (cookie != null)
						{
							cookie.setMaxAge(3600);
							response.addCookie(cookie);
						}
						newUser = toDoListDAO.getUser(newUser.getId());
						request.getSession().setAttribute("userID", newUser.getId());
						request.getSession().setAttribute("userName", newUser.getName());
						request.getSession().setAttribute("admin", newUser.getIsAdmin());
						dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
						dispatcher.forward(request, response);
					}
					else
					{
						request.setAttribute("userMessage", "User not exist");
						dispatcher = getServletContext().getRequestDispatcher("/register.jsp");
						dispatcher.forward(request, response);
					}
				}
				else
				{
					request.setAttribute("userMessage", "Wrong username or password");
					dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch (ToDoListDAOException e)
			{
				request.setAttribute("userMessage", e.getMessage());
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Logout Connected User
		case "/logout":
		{
			request.getSession().invalidate();
			dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
			dispatcher.forward(request, response);
			break;
		}

			// Home page, this page is a starting page
		default:
		case "/home":
		{
			int userID = 0;
			if (request.getSession().getAttribute("userID") == null)
			{
				Cookie[] cookies = request.getCookies();
				if (cookies != null)
				{
					for (Cookie cookie : cookies)
					{
						if (cookie.getName().equals("userID"))
						{
							request.getSession().setAttribute("userID", cookie.getValue());
							userID = Integer.parseInt(cookie.getValue());
							request.setAttribute("userMessage", "Connected using cookies");
							break;
						}
					}
				}
			}
			else
			{
				userID = (int) request.getSession().getAttribute("userID");
			}
			if (userID != 0)
			{
				User user = null;
				try
				{
					user = toDoListDAO.getUser(userID);
				}
				catch (ToDoListDAOException e)
				{
					request.setAttribute("userMessage", e.getMessage());
					dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
					dispatcher.forward(request, response);
				}
				if (user != null)
				{
					request.getSession().setAttribute("userID", user.getId());
					request.getSession().setAttribute("userName", user.getName());
					request.getSession().setAttribute("admin", user.getIsAdmin());
					dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					request.setAttribute("userMessage", "Couldnt find your profile, try to register.");
					dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
					dispatcher.forward(request, response);
				}
			}
			else
			{
				dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
				dispatcher.forward(request, response);
			}

			break;
		}

			// Forwarding to Login page
		case "/loginPage":
		{
			try
			{
				dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
				dispatcher.forward(request, response);
			}
			catch (ServletException e)
			{
				e.printStackTrace();
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Forwording To register.jsp
		case "/registerPage":
		{
			try
			{
				dispatcher = getServletContext().getRequestDispatcher("/register.jsp");
				dispatcher.forward(request, response);
			}
			catch (ServletException e)
			{
				e.printStackTrace();
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Forwording To addItem.jsp
		case "/addItemPage":
		{
			try
			{
				dispatcher = getServletContext().getRequestDispatcher("/addItem.jsp");
				dispatcher.forward(request, response);
			}
			catch (ServletException e)
			{
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Add Item Of Connected User Item list
		case "/addItem":
		{
			String message = null;
			try
			{
				String itemTitle = request.getParameter("itemTitle");
				String itemDescription = request.getParameter("itemDescription");
				String itemDate = request.getParameter("itemDate");
				String itemTime = request.getParameter("itemTime");
				Date itemDateTime = null;
				if (itemDate != null && itemTime != null && itemDate != "" && itemTime != "")
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy/HH:mm:ss");
					try
					{
						itemDateTime = dateFormat.parse(String.format("%s/%s", itemDate, itemTime));
					}
					catch (ParseException e)
					{
						dateFormat = new SimpleDateFormat("dd-MM-yyyy/HH:mm");
						try
						{
							itemDateTime = dateFormat.parse(String.format("%s/%s", itemDate, itemTime));
						}
						catch (ParseException e1)
						{
							request.setAttribute("userMessage", e.getMessage());
							dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
							dispatcher.forward(request, response);
						}
					}
				}
				else
				{
					itemDateTime = new Date();
				}
				int userIntID = (int) (request.getSession().getAttribute("userID"));

				Item newItem = new Item(itemTitle, itemDescription, itemDateTime);
				newItem.setUserID(userIntID);
				if (toDoListDAO.addItem(newItem))
				{
					dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					message = "An error occured trying to add new item.";
					request.getServletContext().setAttribute("userMessage", message);
					dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch (ToDoListDAOException e)
			{
				request.setAttribute("userMessage", e.getMessage());
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Forwording To updateItem.jsp
		case "/updateItemPage":
		{
			try
			{
				int itemID = Integer.parseInt(request.getParameter("itemID"));
				Item item;
				try
				{
					item = toDoListDAO.getItem(itemID);
					
					request.setAttribute("updateItemTitle", item.getTitle());
					request.setAttribute("updateItemDescription", item.getDescription());
					request.setAttribute("updateItemDate", item.getDateString());
					request.setAttribute("updateItemID", item.getId());
					//request.setAttribute("isAdmin", toDoListDAO.getUser(item.getUserID()).getIsAdmin());
				}
				catch (ToDoListDAOException e)
				{
					request.setAttribute("userMessage", e.getMessage());
					dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
					dispatcher.forward(request, response);
				}
				dispatcher = getServletContext().getRequestDispatcher("/updateItem.jsp");
				dispatcher.forward(request, response);
			}
			catch (ServletException e)
			{
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

			// Update Item Of Connected User From Item list
		case "/updateItem":
		{
			try
			{
				String itemTitle = request.getParameter("itemTitle");
				String itemDescription = request.getParameter("itemDescription");
				String itemDate = request.getParameter("itemDate");
				String itemTime = request.getParameter("itemTime");
				Date itemDateTime = null;
				String itemID = (String) request.getParameter("itemID");
				String userID = String.valueOf(request.getSession().getAttribute("userID"));
				int userIntID = Integer.parseInt(userID);
				if (itemDate != null && itemTime != null && itemDate != "" && itemTime != "")
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy/HH:mm:ss");
					try
					{
						itemDateTime = dateFormat.parse(String.format("%s/%s", itemDate, itemTime));
					}
					catch (ParseException e)
					{
						dateFormat = new SimpleDateFormat("dd-MM-yyyy/HH:mm");
						try
						{
							itemDateTime = dateFormat.parse(String.format("%s/%s", itemDate, itemTime));
						}
						catch (ParseException e1)
						{
							request.setAttribute("userMessage", e.getMessage());
							dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
							dispatcher.forward(request, response);
						}
					}
				}
				else
				{
					itemDateTime = new Date();
				}
				Item updateItem = new Item(itemTitle, itemDescription, itemDateTime);
				updateItem.setId(Integer.parseInt(itemID));
				updateItem.setUserID(userIntID);
				// test if itemTitle already exists
				if (toDoListDAO.updateItem(updateItem))
				{
					request.setAttribute("updateItem", updateItem.getId());
					dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					request.setAttribute("userMessage", "Item title not exist");
					dispatcher = getServletContext().getRequestDispatcher("/addItem.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch (ToDoListDAOException e)
			{
				request.setAttribute("userMessage", e.getMessage());
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;

		}

			// Delete Item Of Connected User From Item list
		case "/deleteItem":
		{
			try
			{
				String itemID = request.getParameter(String.valueOf("itemID"));

				Item deletedItem = toDoListDAO.getItem(Integer.parseInt(itemID));
				int userID = (int) request.getSession().getAttribute("userID");
				if (toDoListDAO.connect(userID))
				{
					if (toDoListDAO.deleteItem(deletedItem))
					{
						request.setAttribute("userMessage", "Deleted" + deletedItem.toShortString());
						dispatcher = getServletContext().getRequestDispatcher("/currentUserHomePage.jsp");
						dispatcher.forward(request, response);
					}
					else
					{
						request.setAttribute("userMessage", "item was not deleted");
						dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
						dispatcher.forward(request, response);
					}
				}
				else
				{
					request.setAttribute("userMessage", "Invalid user ID");
					dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch (ToDoListDAOException e)
			{
				request.setAttribute("userMessage", e.getMessage());
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}

		case "/adminPage":
		{
			try
			{
				dispatcher = getServletContext().getRequestDispatcher("/adminPage.jsp");
				dispatcher.forward(request, response);
			}
			catch (ServletException e)
			{
				e.printStackTrace();
				dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
				dispatcher.forward(request, response);
			}
			break;
		}
		}

	}

	/**
	 * Checks if is alpha numeric.
	 *
	 * @param str
	 *            the str
	 * @return true, if is alpha numeric
	 */
	private boolean isAlphaNumeric(String str)
	{
		for (int i = 0; i < str.length(); i++)
		{
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && (c != ' '))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Do post.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}