package il.ac.hit.todolistdao.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Attributes;

/**
 * Application Lifecycle Listener implementation class AdminControl
 *
 */
@WebListener
public class AdminControl implements HttpSessionListener, HttpSessionAttributeListener
{

	/**
	 * Default constructor.
	 */
	public AdminControl()
	{
	}

	/**
	 * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent arg0)
	{
		HttpSession session = arg0.getSession();
		ServletContext application = session.getServletContext();
		if (application.getAttribute("activeSessions") == null)
		{
			application.setAttribute("activeSessions", new HashSet<HttpSession>());
		}

		if (application.getAttribute("createdSessions") == null)
		{
			application.setAttribute("createdSessions", new HashSet<HttpSession>());
		}
		@SuppressWarnings("unchecked")
		Set<HttpSession> activeSet = (Set<HttpSession>) application.getAttribute("activeSessions");
		activeSet.add(session);
		application.setAttribute("activeSessionsCount", activeSet.size());
		@SuppressWarnings("unchecked")
		Set<HttpSession> createdSet = (Set<HttpSession>) application.getAttribute("createdSessions");
		createdSet.add(session);
		application.setAttribute("createdSessionsCount", createdSet.size());
	}

	/**
	 * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent arg0)
	{
		HttpSession session = arg0.getSession();
		ServletContext application = session.getServletContext();

		if (application.getAttribute("destroyedSessions") == null)
		{
			application.setAttribute("destroyedSessions", new HashSet<HttpSession>());
		}
		
		if(application.getAttribute("activeSessions")==null)
		{
			application.setAttribute("activeSessions", new HashSet<HttpSession>());
		}
		
		@SuppressWarnings("unchecked")
		Set<HttpSession> activeSet = (HashSet<HttpSession>) application.getAttribute("activeSessions");
		activeSet.remove(session);
		application.setAttribute("activeSessionsCount", activeSet.size());
		@SuppressWarnings("unchecked")
		Set<HttpSession> destroyedSet = (Set<HttpSession>) application.getAttribute("destroyedSessions");
		destroyedSet.add(session);
		application.setAttribute("destroyedSessionsCount", destroyedSet.size());
	}

	/**
	 * @see HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
	 */
	public void attributeAdded(HttpSessionBindingEvent arg0)
	{
		HttpSession session = arg0.getSession();
		ServletContext application = session.getServletContext();

		if (application.getAttribute("addedAttributes") == null)
		{
			application.setAttribute("addedAttributes", new HashMap<>());
		}
		
		if(application.getAttribute("activeAttributes") == null)
		{
			application.setAttribute("activeAttributes", new HashMap<>());
		}
		
		@SuppressWarnings("unchecked")
		Map<String, Object> addedAttributes = (Map<String, Object>) application.getAttribute("addedAttributes");
		addedAttributes.put(arg0.getName(), arg0.getValue());
		application.setAttribute("addedAttributesCount", addedAttributes.size());
		
		@SuppressWarnings("unchecked")
		Map<String, Object> activeAttributes = (Map<String, Object>) application.getAttribute("activeAttributes");
		activeAttributes.put(arg0.getName(), arg0.getValue());
		application.setAttribute("activeAttributesCount", activeAttributes.size());
	}

	/**
	 * @see HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
	 */
	public void attributeRemoved(HttpSessionBindingEvent arg0)
	{
		HttpSession session = arg0.getSession();
		ServletContext application = session.getServletContext();
		
		if(application.getAttribute("removedAttributes") == null)
		{
			application.setAttribute("removedAttributes", new HashMap<>());
		}
		
		if(application.getAttribute("activeAttributes") == null)
		{
			application.setAttribute("activeAttributes", new HashMap<>());
		}
		
		
		@SuppressWarnings("unchecked")
		Map<String, Object> attributes = (Map<String, Object>) application.getAttribute("removedAttributes");
		attributes.put(arg0.getName(), arg0.getValue());
		application.setAttribute("removedAttributesCount", attributes.size());
		
		@SuppressWarnings("unchecked")
		Map<String, Object> activeAttributes = (Map<String, Object>) application.getAttribute("activeAttributes");
		activeAttributes.remove(arg0.getName());
		application.setAttribute("activeAttributesCount", activeAttributes.size());
	}

	/**
	 * @see HttpSessionAttributeListener#attributeReplaced(HttpSessionBindingEvent)
	 */
	public void attributeReplaced(HttpSessionBindingEvent arg0)
	{
		HttpSession session = arg0.getSession();
		ServletContext application = session.getServletContext();
		
		if(application.getAttribute("replacedAttributes") == null)
		{
			application.setAttribute("replacedAttributes", new HashMap<>());
		}
		
		if(application.getAttribute("activeAttributes") == null)
		{
			application.setAttribute("activeAttributes", new HashMap<>());
		}
		
		@SuppressWarnings("unchecked")
		Map<String, Object> replacedAttributes = (Map<String, Object>) application.getAttribute("replacedAttributes");
		replacedAttributes.put(arg0.getName(), arg0.getValue());
		application.setAttribute("replacedAttributesCount", replacedAttributes.size());
	}
}