package il.ac.hit.todolistdao.model;

/**
 * A class that represents the project Exceptions.
 */
public class ToDoListDAOException extends Exception
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new hibernate to do list dao exception.
	 */
	public ToDoListDAOException()
	{
		super();
	}

	/**
	 * Instantiates a new hibernate to do list dao exception.
	 *
	 * @param arg0 the arg0
	 */
	public ToDoListDAOException(String arg0)
	{
		super(arg0);
	}

	/**
	 * Instantiates a new hibernate to do list dao exception.
	 *
	 * @param arg0 the arg0
	 */
	public ToDoListDAOException(Throwable arg0)
	{
		super(arg0);
	}

	/**
	 * Instantiates a new hibernate to do list dao exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public ToDoListDAOException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	/**
	 * Instantiates a new hibernate to do list dao exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 * @param arg3 the arg3
	 */
	public ToDoListDAOException(String arg0, Throwable arg1, boolean arg2, boolean arg3)
	{
		super(arg0, arg1, arg2, arg3);
	}
}