package il.ac.hit.todolistdao.model;

//import java.util.Date;

public class Program
{
//	public static void main(String[] args) throws InterruptedException
//	{
//		HibernateToDoListDAO toDoList = HibernateToDoListDAO.getInstance();
//
//		User ohad = new User("ohad", "co");
//		User nathan = new User("nathan", "321");
//
//
//		Item oitem = new Item("test1", "checking1", new Date(), Item.ACTIVE);
//		Item ooitem = new Item("test2", "checking2", new Date(), Item.FINISHED);
//		Item nitem = new Item("test3", "checking3", new Date(), Item.FINISHED);
//		Item nnitem = new Item("test4", "checking4", new Date(), Item.ACTIVE);
//		oitem.setUserID(ohad.getId());
//		ooitem.setUserID(ohad.getId());
//		nitem.setUserID(nathan.getId());
//		nnitem.setUserID(nathan.getId());
//
//		try
//		{
//			toDoList.connect(ohad);
//			toDoList.register(nathan.getName(), nathan.getPassword());
//			toDoList.addItem(ooitem);
//			toDoList.addItem(nnitem);
//			toDoList.addItem(nitem);
//			toDoList.addItem(oitem);
//			
//			//toDoList.ShowItems(ohad.getId());
//		}
//		catch (HibernateToDoListDAOException e)
//		{
//			System.out.println(e.getMessage());
//		}
//	}
}



//<?xml version='1.0' encoding='utf-8'?>
//<!DOCTYPE hibernate-configuration PUBLIC "-//Hibernate/Hibernate Configuration DTD//EN" 
//"http://hibernate.sourceforge.net/hibernate-configuration-3.0.dtd">
//<hibernate-configuration>
//	<session-factory>
//		<property name="hibernate.connection.driver_class">
//			com.mysql.jdbc.Driver
//		</property>
//		<property name="hibernate.connection.url">
//			jdbc:mysql://localhost:3010/todolist
//		</property>
//		<property name="hibernate.connection.username">tod</property>
//		<property name="hibernate.connection.password">olist</property>
//		<property name="show_sql">true</property>
//		<property name="dialect">org.hibernate.dialect.MySQLDialect</property>
//		<property name="hibernate.hbm2ddl.auto">update</property>
//		<!-- Mapping files -->
//		<mapping resource="mapping.xml" />
//	</session-factory>
//</hibernate-configuration>