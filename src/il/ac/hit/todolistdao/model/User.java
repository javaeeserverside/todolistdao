package il.ac.hit.todolistdao.model;

/**
 * Created by Ohad on 23/12/2015.
 */
public class User
{
	
	/** The name. */
	private String name;
	
	/** The password. */
	private String password;
	
	/** The id. */
	private int id;
	
	/** The is admin. */
	private boolean isAdmin;

	/**
	 * Default C'tor that initialize this class members with default values.
	 */
	public User()
	{
		setName(null);
		setPassword(null);
		setIsAdmin(false);
	}

	/**
	 * Full member C'tor that initialize the todolist User with the values it
	 * receives from the user.
	 *
	 * @param name
	 *            the name
	 * @param password
	 *            the password
	 */
	public User(String name, String password)
	{
		super();
		setName(name);
		setPassword(password);
		setIsAdmin(false);
	}

	/**
	 * A get property method for the id of the User.
	 *
	 * @return the id
	 */
	public int getId()
	{
		return this.hashCode();
	}

	/**
	 * A set property for the id of the User.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id)
	{
		this.id = this.hashCode();
	}

	/**
	 * A get property method for the password of the User.
	 *
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * A set property for the password of the User.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * A get property method for the name of the User.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * A set property for the name of the User.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Checks if the user is admin.
	 *
	 * @return true, if is admin
	 */
	public boolean getIsAdmin()
	{
		return isAdmin;
	}

	/**
	 * Sets weather the user is the admin.
	 *
	 * @param isAdmin the new admin
	 */
	public void setIsAdmin(boolean isAdmin)
	{
		this.isAdmin = isAdmin;
	}

	/**
	 * Overriding toString from Object.
	 *
	 * @return the String representation of User.
	 */
	@Override
	public String toString()
	{
		return "User [name=" + name + ", password=" + password + ", id=" + getId() + "]";
	}

	/**
	 * Overriding hasCode from Object.
	 *
	 * @return An integer that represents the user.
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result += prime * result + ((name == null) ? 0 : name.hashCode());
		result += prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	/**
	 * Overriding equals from Object.
	 *
	 * @param obj the obj
	 * @return true if the users equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		User other = (User) obj;
		return (name.contentEquals(other.name) && password.contentEquals(other.password));
	}
}