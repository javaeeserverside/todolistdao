package il.ac.hit.todolistdao.model;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import java.util.List;

/**
 * A class that represents a todo list object.
 */
public class ToDoListDAO implements IToDoListDAO
{

	/** The instance. */
	private static ToDoListDAO instance;

	/** The factory. */
	private SessionFactory factory = new AnnotationConfiguration().configure().buildSessionFactory();

	/**
	 * Instantiates a new hibernate to do list dao.
	 */
	private ToDoListDAO()
	{
	}

	/**
	 * Gets the single instance of HibernateToDoListDAO.
	 *
	 * @return single instance of HibernateToDoListDAO
	 */
	public static synchronized ToDoListDAO getInstance()
	{
		if (instance == null)
		{
			instance = new ToDoListDAO();
		}

		return instance;
	}

	/**
	 * Getting item from user and upload to server.
	 *
	 * @param item
	 *            represents the item to add
	 * @return if Item (original) was added or not at the server
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	@Override
	public boolean addItem(Item item) throws ToDoListDAOException
	{
		boolean success = true;
		if (connect(item.getUserID()))
		{
			if (item != null && item.getTitle() != "")
			{
				Session session = null;
				try
				{
					session = factory.openSession();
					session.beginTransaction();
					List<Item> items = session.createQuery("from Item where userID =" + item.getUserID()).list();

					for (Item i : items)
					{
						if (i.equals(item))
						{
							throw new ToDoListDAOException("ERROR: Trying to add an existing item!");
						}
					}

					session.save(item);
					session.getTransaction().commit();
					return success;
				}
				catch (HibernateException e)
				{
					if (session.getTransaction() != null)
					{
						session.getTransaction().rollback();
					}
					else
					{
						throw new ToDoListDAOException("Error: No transaction!");
					}
				}
				finally
				{
					session.close();
				}
				return success;
			}
			else
			{
				throw new ToDoListDAOException("Error: An item must contain a title.");
			}
		}
		else
		{
			throw new ToDoListDAOException("Error: User doesnt exist");
		}
	}

	/**
	 * Getting item from user to update the existing one.
	 *
	 * @param item
	 *            from user
	 * @return if Item (original) was updated or not at the server
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	@Override
	public boolean updateItem(Item item) throws ToDoListDAOException
	{
		boolean success = true;
		if (connect(item.getUserID()))
		{
			Session session = null;
			try
			{
				session = factory.openSession();
				session.beginTransaction();

				Item tempInstanceOfItem = (Item) session.load(Item.class, item.getId());
				if (tempInstanceOfItem != null && tempInstanceOfItem.getUserID() == item.getUserID())
				{
					if (item.getDate() != null)
					{
						tempInstanceOfItem.setDate(item.getDate());
					}
					if (item.getDescription() != null)
					{
						tempInstanceOfItem.setDescription(item.getDescription());
					}
					if (item.getTitle() != null)
					{
						tempInstanceOfItem.setTitle(item.getTitle());
					}
					session.save(tempInstanceOfItem);
					session.getTransaction().commit();
				}
				else
				{
					throw new ToDoListDAOException("Update Error: You cannot change this item.");
				}
			}
			catch (HibernateException e)
			{
				success = false;
				if (session.getTransaction() != null)
				{
					session.getTransaction().rollback();
				}
				else
				{
					throw new ToDoListDAOException("Error: Transaction is empty");
				}
			}
			finally
			{
				session.close();
			}
			return success;
		}
		else
		{
			System.out.println("no user connected");
			return !success;
		}
	}

	/**
	 * Delete item.
	 *
	 * @param item
	 *            the item
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	public boolean deleteItem(Item item) throws ToDoListDAOException
	{
		if(connect(item.getUserID()))
		{
			boolean success = true;
			Session session = null;
			try
			{
				session = factory.openSession();
				session.beginTransaction();

				Item tempInstance = (Item) session.load(Item.class, item.getId());
				session.delete(tempInstance);
				session.getTransaction().commit();
				return success;
			}
			catch (HibernateException e)
			{
				throw new ToDoListDAOException("Could't delete item");
			}
			finally
			{
				session.close();
			}
		}
		else
		{
			throw new ToDoListDAOException("User doesnt exist!");
		}
	}

	/**
	 * an implementation of connect of the IToDoListUserDAO interface.
	 *
	 * @param user
	 *            the user
	 * @return true, if successfull
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	public boolean connect(User user) throws ToDoListDAOException
	{
		return connect(user.getId());
	}

	/**
	 * an overloading of connect of the IToDoListUserDAO interface.
	 *
	 * @param user
	 *            the user
	 * @return true, if successfull
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	public boolean connect(int userID) throws ToDoListDAOException
	{
		Session session = factory.openSession();
		session.beginTransaction();
		List<User> users = session.createQuery("from User").list();
		session.close();

		for (User i : users)
		{
			if (i.getId() == userID)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * an overloading of connect of the IToDoListUserDAO interface.
	 *
	 * @param i_User
	 *            the i_ user
	 * @return true, if successfull
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	public boolean connect(String name, String password) throws ToDoListDAOException
	{
		return connect(new User(name, password));
	}

	/**
	 * an implementation of register of the IToDoListUserDAO interface.
	 *
	 * @param user
	 * @return true, if successfull
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	public boolean register(User user) throws ToDoListDAOException
	{
		return register(user.getName(), user.getPassword());
	}

	/**
	 * an implementation of register of the IToDoListUserDAO interface.
	 *
	 * @param user
	 * @return true, if successfull
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	public boolean register(String name, String password) throws ToDoListDAOException
	{
		boolean success = true;
		Session session = null;

		if (!connect(name, password))
		{
			try
			{
				session = factory.openSession();
				session.beginTransaction();
				User newUser = new User(name, password);
				session.save(newUser);
				session.getTransaction().commit();
				return success;
			}
			catch (HibernateException e)
			{
				if (session.getTransaction() != null)
				{
					session.getTransaction().rollback();
				}
				else
				{
					throw new ToDoListDAOException("Failed to open a session to add user");
				}
			}
			finally
			{
				session.close();
			}
		}
		else
		{
			throw new ToDoListDAOException("User already exist");
		}
		return success;
	}

	/**
	 * Setting the user it receives as 'user' to admin control.
	 *
	 * @param user
	 *            the user
	 * @return if User (original) was updated or not at the server
	 * @throws ToDoListDAOException
	 *             the to do list dao exception
	 */
	@Override
	public boolean setAdmin(User user) throws ToDoListDAOException
	{
		boolean success = true;
		if (connect(user.getId()))
		{
			Session session = null;
			try
			{
				session = factory.openSession();
				session.beginTransaction();

				User tempInstanceOfUser = (User) session.load(User.class, user.getId());
				if (tempInstanceOfUser != null && tempInstanceOfUser.getId() == user.getId())
				{
					tempInstanceOfUser.setIsAdmin(true);
					session.save(tempInstanceOfUser);
					session.getTransaction().commit();
				}
				else
				{
					throw new ToDoListDAOException("Update Error: You cannot change this user.");
				}
			}
			catch (HibernateException e)
			{
				success = false;
				if (session.getTransaction() != null)
				{
					session.getTransaction().rollback();
				}
				else
				{
					throw new ToDoListDAOException("Error: Transaction is empty");
				}
			}
			finally
			{
				session.close();
			}
			return success;
		}
		else
		{
			throw new ToDoListDAOException("no user connected");
		}
	}

	/**
	 * an implementation of getItems of the IToDoListDAO interface.
	 *
	 * @param user
	 *            the user
	 * @return The items of the user
	 * @throws ToDoListDAOException
	 *             the hibernate to do list dao exception
	 */
	@Override
	public List<Item> getItems(User user) throws ToDoListDAOException
	{
		return getItems(user.getId());
	}

	/**
	 * Gets the items.
	 *
	 * @param userID
	 *            the user id
	 * @return the items
	 */
	public List<Item> getItems(int userID) throws ToDoListDAOException
	{
		Session session = factory.openSession();
		session.beginTransaction();
		List<Item> items = session.createQuery("from Item where userID = " + userID).list();
		session.close();

		return items;
	}

	/**
	 * Gets the user by the user id
	 *
	 * @param userID
	 *            the user id
	 * @return the user
	 * @throws ToDoListDAOException
	 */
	public User getUser(int userID) throws ToDoListDAOException
	{
		try
		{
			Session session = factory.openSession();
			session.beginTransaction();
			List<User> users = session.createQuery("from User where ID = " + userID).list();
			session.close();

			if (users.size() == 1)
			{
				return users.get(0);
			}
			return null;
		}
		catch (HibernateException e)
		{
			throw new ToDoListDAOException(e.getMessage());
		}
	}

	/**
	 * Gets the item by the id.
	 *
	 * @param itemID
	 *            the item id
	 * @return the item
	 */
	public Item getItem(int itemID)
	{
		try
		{
			Session session = factory.openSession();
			session.beginTransaction();
			List<Item> items = session.createQuery("from Item where id=" + itemID).list();
			session.close();

			if (items.size() == 1)
			{
				return items.get(0);
			}
			return null;
		}
		catch (HibernateException e)
		{
			throw new HibernateException(e.getMessage());
		}
	}
}