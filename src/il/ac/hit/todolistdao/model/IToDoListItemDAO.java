package il.ac.hit.todolistdao.model;

import java.util.List;
import il.ac.hit.todolistdao.model.User;

/**
 * An interface representing a TODO list DAO item.
 */
public interface IToDoListItemDAO
{
	
	/**
	 * method AddItem adds a new item to the DB.
	 *
	 * @param item the item
	 * @return true if the Item was added successfully, false otherwise.
	 * @throws ToDoListDAOException the to do list dao exception
	 */
    boolean addItem(Item item) throws ToDoListDAOException;
    
    /**
     * method UpdateItem updates the Items with new data.
     *
     * @param item the item
     * @return true if the item exists and updated successfully, false otherwise.
     * @throws ToDoListDAOException the to do list dao exception
     */
    boolean updateItem(Item item) throws ToDoListDAOException;
    
    /**
     * method DeleteItem deletes an item by ID from the DB .
     *
     * @param id The id of the item
     * @return true if the Item was deleted successfully, false otherwise.
     * @throws ToDoListDAOException the to do list dao exception
     */
    boolean deleteItem(Item item) throws ToDoListDAOException;
    
    /**
     * method getItems is getting the items for the current user.
     *
     * @param user the user
     * @return List of items for the user presented with the id
     * @throws ToDoListDAOException the to do list dao exception
     */
    List<Item> getItems(User user) throws ToDoListDAOException;
    
     /**
      * This method Gets the item from the DB with the id it receives.
      *
      * @param itemID the item id
      * @return the item
      * @throws ToDoListDAOException the to do list dao exception
      */
     Item getItem(int itemID) throws ToDoListDAOException;;
}