package il.ac.hit.todolistdao.model;

/**
 * The Interface IToDoListDAO.
 */
public interface IToDoListDAO extends IToDoListItemDAO, IToDoListUserDAO
{

}
