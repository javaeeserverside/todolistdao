package il.ac.hit.todolistdao.model;

/**
 * An interface representing a TODO list DAO User.
 */
public interface IToDoListUserDAO
{
	
	/**
	 * method register adds a new user to the DB.
	 *
	 * @param name            The name of the user.
	 * @param password            The password of the user.
	 * @return true if the User was added successfully, false otherwise.
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	boolean register(String name, String password) throws ToDoListDAOException;

	/**
	 * method connect checks of the user exist in the DB.
	 *
	 * @param name            The name of the user.
	 * @param password            The password of the user.
	 * @return true if the User exist in the DB, false otherwise.
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	boolean connect(String name, String password) throws ToDoListDAOException;
	
	/**
	 * A Connect method that checks if the user User is registered in the database.
	 *
	 * @param user the user
	 * @return true, if successful
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	boolean connect(User user) throws ToDoListDAOException;
	
	/**
	 * method connect checks of the user exist in the DB.
	 *
	 * @param userID            The id of the user.
	 * @return true if the User exist in the DB, false otherwise.
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	boolean connect(int userID) throws ToDoListDAOException;
	
	/**
	 * Gets the user represented by the id from the database.
	 *
	 * @param userID the user id
	 * @return the user
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	User getUser(int userID) throws ToDoListDAOException;
	
	/**
	 * A Register method inserts a new user to the database.
	 *
	 * @param user the user
	 * @return true, if successful
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	boolean register(User user) throws ToDoListDAOException;
	
	/**
	 * Update an existing user in the data base.
	 *
	 * @param user the user
	 * @return true, if successful
	 * @throws ToDoListDAOException the to do list dao exception
	 */
	boolean setAdmin(User user) throws ToDoListDAOException;
}