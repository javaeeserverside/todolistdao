package il.ac.hit.todolistdao.model;

import java.util.Date;

/**
 * A class that represents a to do list item
 */
public class Item
{
	private String title;
	private String description;
	private Date date;
	public static final boolean ACTIVE = true;
	public static final boolean FINISHED = false;
	private boolean itemState;;
	private int id;
	private int userID;

	/**
     * Default C'tor that initialize this class members with default values.
     */
	public Item()
	{
		setId(0);
		setTitle(null);
		setDescription(null);
		setDate(null);
		setItemState(false);
	}

	/**
     * A partial C'tor that initialize Item values
     */
	public Item(int id, String title)
	{
		setId(id);
		setTitle(title);
		setItemState(false);
		setDate(null);
		setDescription(null);
	}

	/**
     * Partial member C'tor that initialize the todolist Item with the values it
     * receives from the user.
     */
	public Item(String title, String description, Date date)
	{
		setTitle(title);
		setDescription(description);
		setDate(date);
		setItemState(itemState);
	}

	/**
     * A set property for the id of the Item
     * 
     * @param id - The id of the item.
     */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
     * A get property for the id of the item. Returns an integer that represents
     * the item id.
     * 
     * @return The item id.
     */
	public int getId()
	{
		return this.id;
	}

	/**
     * A set property method for the title of the item.
     * 
     * @param title - The item title.
     */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
     * A get property method for the title of the item.
     * 
     * @return String - The title of the item.
     */
	public String getTitle()
	{
		return title;
	}

	/**
     * A set property method for the description of the item.
     * 
     * @param description - The description of the item.
     */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
     * A get property method for the description of the item.
     * 
     * @return String - The description of the item.
     */
	public String getDescription()
	{
		return description;
	}

	/**
     * A set property for the item date.
     * in case of a null value, initial with the current date.
     * 
     * @param date - The date the item is set for.
     */
	public void setDate(Date date)
	{
		if(date == null)
		{
			date = new Date();
		}

		this.date = date;
	}

	/**
     * A get property method for the date of the item.
     * 
     * @return The date the item is set for.
     */
	public Date getDate()
	{
		return date;
	}

	/**
	 * A toString for the date element in item.
	 * 
	 * @return a representation of time as dd-MM-yyyy
	 */
	public String getDateString()
	{
		String[] dateTimeParts = getDate().toString().split(" ");
		String[] dateParts = dateTimeParts[0].split("-");
		String date = dateParts[2] + "-" +dateParts[1] +"-"+dateParts[0]+" "+dateTimeParts[1];
		return date;
	}
	
	/**
     * A set property method for the item state.
     * 
     * @param itemState - A state for the item.
     */
	public void setItemState(boolean itemState)
	{
		itemState =  new Date().compareTo(this.getDate()) < 0 ? Item.ACTIVE : Item.FINISHED;
	}

	/**
     * A get property item for the item state
     * 
     * @return boolean value of the item state (active/finished).
     */
	public boolean getItemState()
	{
		return new Date().compareTo(this.getDate()) < 0 ? Item.ACTIVE : Item.FINISHED;
	}

	/**
     * A get property method for the id of the user owns the item
     * 
     * @return Integer - the user id.
     */
	public int getUserID()
	{
		return userID;
	}

	public void setUserID(int userID)
	{
		this.userID = userID;
	}

	/**
	 * Overriding hasCode from Object.
	 *
	 * @return An integer that represents the item.
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result += prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	/**
	 * Overriding equals from Object.
	 *
	 * @return true if the items equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		Item other = (Item) obj;

		if(this.description.contentEquals(other.description) && this.title.contentEquals(other.title))
		{
			return true;
		}

		return false;
	}

	/**
	 * Overriding toString from Object.
	 *
	 * @return the String representation of Item.
	 */
	@Override
	public String toString()
	{
		return "Item [title=" + title + ", description=" + description + ", date=" + date + ", itemState=" + itemState +
			   ", id=" + id + ", User id=" + userID + "]";
	}
	
	/**
	 * A method that return the title and description of the Item.
	 * 
	 * @return A string representation of the Item.
	 */
	public String toShortString()
	{
		return "Item: title=" + title + ", description=" + description;
	}
}