package il.ac.hit.todolistdao.view;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

import il.ac.hit.todolistdao.model.ToDoListDAO;
import il.ac.hit.todolistdao.model.ToDoListDAOException;
import il.ac.hit.todolistdao.model.Item;

import java.io.*;
import java.util.*;

/**
 * @author Ohad Cohen
 */
public class Items extends SimpleTagSupport
{

	private int userID;
	private boolean isPast = false;
	private boolean isFuture = false;
	
	
	
	public boolean getIsPast()
	{
		return isPast;
	}

	public void setIsPast(boolean isPast)
	{
		this.isPast = isPast;
	}

	public boolean getIsFuture()
	{
		return isFuture;
	}

	public void setIsFuture(boolean isFuture)
	{
		this.isFuture = isFuture;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userid
	 *            the new user id
	 */
	public void setUserID(int userid)
	{
		this.userID = userid;
	}

	private List<Item> items;

	/*
	 * An implemetation of the doTag from SimpleTagSupport.
	 * 
	 * generates the items table of the user.
	 */
	public void doTag() throws JspException, IOException
	{
		try
		{
			items = ToDoListDAO.getInstance().getItems(userID);
		}
		catch (ToDoListDAOException e)
		{
			e.printStackTrace();
		}

		JspWriter out = getJspContext().getOut();
		StringBuilder builder = new StringBuilder();
		builder.append("<table class='tbl'>");
		builder.append("<tr><td><b>Delete</b></td><td><b>Title</b></td>");
		builder.append("<td><b>Description</b></td><td><b>Date</b></td>");
		builder.append("<td><b>Time</b></td><td><b>State</b></td>");
		builder.append("<td><b>Update</b></td></tr>");

		if (items != null)
		{
			for (Item item : items)
			{
				if(getIsPast() == true && item.getItemState() == Item.FINISHED)
				{
					builder.append(getRow(item));
				}
				else if(getIsFuture() == true && item.getItemState() == Item.ACTIVE)
				{
					builder.append(getRow(item));
				}
				else if(getIsFuture() == false && getIsPast() == false)
				{
					builder.append(getRow(item));
				}
			}
			
			builder.append("</table>");

			out.print(builder.toString());
		}
	}
	private String getRow(Item item)
	{
		int itemID = item.getId();
		String title = item.getTitle(), description = item.getDescription();
		boolean state = item.getItemState();
		String[] dateStr = item.getDateString().split(" ");
		StringBuilder builder = new StringBuilder();
		builder.append("<tr align='center'>");
		builder.append("<td><form action='/todolistdao/controller/deleteItem'");
		builder.append("method='get'>");
		builder.append("<input type='image' src='../images/DeleteIcon.png' alt='submit'>");
		builder.append("<input name='itemID' type='hidden' value=" + String.valueOf(itemID) + ">");
		builder.append("</form></td><td>" + title + "</td>");
		builder.append("<td>" + description + "</td>");
		builder.append("<td>" + String.valueOf(dateStr[0]) + "</td>");
		builder.append("<td>" + String.valueOf(dateStr[1]) + "</td>");
		String str = String.valueOf(state).equals("true") ? "Active" : "Finished";
		builder.append("<td>" + str + "</td>");
		builder.append("<td>");
		builder.append("<form action='/todolistdao/controller/updateItemPage'");
		builder.append("method='get'>");
		builder.append("<input type='image' src='../images/UpdateIcon.png' alt='submit'>");
		builder.append("<input name='itemID' type='hidden'");
		builder.append("value=" + String.valueOf(itemID) + ">");
		builder.append("</form></td></tr>");
		return builder.toString();
	}
}