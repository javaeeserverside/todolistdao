<%@ page language="java" contentType="text/html; charset=windows-1255"
	import="java.util.*" pageEncoding="windows-1255"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
<title>Personal ToDo List</title>
<link rel="stylesheet" type="text/css" href="../css/tables.css" />
<link rel="stylesheet" type="text/css" href="../css/button.css" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>
<body>
	<%
		int userID = (int) session.getAttribute("userID");
		String userName = (String) session.getAttribute("userName");
		boolean isAdmin = (session.getAttribute("admin")) == null ? false
				: ((boolean) session.getAttribute("admin"));
		
		
		if (userName != null)
		{
	%>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Actions <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/todolistdao/controller/addItemPage">Add Item</a></li>
						<li role="separator" class="divider"></li>
						<%
							if (isAdmin)
								{
						%>
						<li><a href="/todolistdao/controller/adminPage">Admin Page</a></li>
						<%
							}
						%>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/todolistdao/controller/home">Home</a></li>
				<li><a href="/todolistdao/controller/logout">Logout(<%=userName%>)    
				</a></li>
			</ul>
		</div>
	</nav>
<br><br><br><br>

	<%
		}
	%>
	
	<%
		if (request.getAttribute("userMessage") != null)
		{
			String userMessage = String.valueOf(request.getAttribute("userMessage"));
			//use bean
	%>
	<h4><%=userMessage%></h4>
	<%
		}
	%>

	<div class="container">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
			<li><a data-toggle="tab" href="#past">Past</a></li>
			<li><a data-toggle="tab" href="#future">Future</a></li>
		</ul>

		<div class="tab-content">
			<div id="home" class="tab-pane fade in active">
				<h3>Home</h3>
				<%@ taglib uri="/WEB-INF/tlds/items.tld" prefix="hit"%>
				<hit:itemList userID="<%=userID%>"></hit:itemList>
			</div>
			<div id="past" class="tab-pane fade">
				<h3>Past Items</h3>
				<%@ taglib uri="/WEB-INF/tlds/items.tld" prefix="hit"%>
				<hit:itemList userID="<%=userID%>" isPast="true"></hit:itemList>
			</div>
			<div id="future" class="tab-pane fade">
				<h3>Future Items</h3>
				<%@ taglib uri="/WEB-INF/tlds/items.tld" prefix="hit"%>
				<hit:itemList userID="<%=userID%>" isFuture="true"></hit:itemList>
			</div>
		</div>
	</div>
</body>
</html>