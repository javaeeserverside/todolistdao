<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255" isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/button.css" />
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
	<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<title>Error!</title>
</head>
<body>
<%
		String userName = (String) session.getAttribute("userName");
		boolean isAdmin = (session.getAttribute("admin")) == null ? false
				: ((boolean) session.getAttribute("admin"));
	%>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			<%
							if (isAdmin)
								{
						%>
						<li><a href="/todolistdao/controller/adminPage">Admin Page</a></li>
						<%
							}
						%>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/todolistdao/controller/home">Home</a></li>
				<li><a href="/todolistdao/controller/logout">Logout(<%=userName%>)
				</a></li>
			</ul>
		</div>
	</nav>
	<br><br><br>

<div align="center">
	<%
		String message = (String)request.getAttribute("userMessage");
		if (message != null)
		{
	%>
	<h2><%=message%></h2>
	<%
		}
	%>
	</div>
	<div align="center">
		<img src="../images/error.png">
	</div>
</body>
</html>