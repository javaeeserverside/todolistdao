<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
<title>Update Item</title>
<link rel="stylesheet" type="text/css" href="../css/button.css" />
</head>
<body>
	<%
		String itemOldTitle = (String) request.getAttribute("updateItemTitle");
		String itemOldDescription = (String) request.getAttribute("updateItemDescription");
		String[] itemDateTime = ((String)request.getAttribute("updateItemDate")).split(" ");
		int itemID = (int)request.getAttribute("updateItemID");
		String itemOldDate = itemDateTime[0];
		String itemOldTime = itemDateTime[1];
		String userName = (String) session.getAttribute("userName");
		boolean isAdmin = (session.getAttribute("admin")) == null ? false
				: ((boolean) session.getAttribute("admin"));
	%>
	
	
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			<%
							if (isAdmin)
								{
						%>
						<li><a href="/todolistdao/controller/adminPage">Admin Page</a></li>
						<%
							}
						%>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/todolistdao/controller/home">Home</a></li>
				<li><a href="/todolistdao/controller/logout">Logout(<%=userName%>)
				</a></li>
			</ul>
		</div>
	</nav>
	
	<br><br><br><br><br>
	
	
	<h1 align="center">Update Item</h1>
	<br>
	<form action="/todolistdao/controller/updateItem" method="get"
		class="form-horizontal" role="form">
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemTitle">Title:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemTitle"
					id="itemTitle" value="<%=itemOldTitle%>">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemDescription">Description:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemDescription"
					id="itemDescription" value="<%=itemOldDescription%>">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemDate">Date:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemDate" id="date"
					value="<%=itemOldDate%>">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemTime">Time:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemTime" id="date"
					value="<%=itemOldTime%>">
			</div>
		</div>
		<div class="form-group">
			<div align="center" class="col-sm-offset-1 col-sm-10">
				<button type="submit" class="btn btn-default">Update Item</button>
			</div>
		</div>
		<input type="hidden" name="userID" value=<%=request.getAttribute("userID")%> />
		<input type="hidden" name="itemID" value=<%=request.getAttribute("updateItemID")%> />
	</form>
	

</body>
</html>