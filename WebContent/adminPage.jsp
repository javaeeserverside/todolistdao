<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255" import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/button.css" />
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<title>Admin Page</title>
</head>
<body>

	<%
		String userName = (String) session.getAttribute("userName");
		boolean isAdmin = (session.getAttribute("admin")) == null ? false
				: ((boolean) session.getAttribute("admin"));
	%>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			<%
							if (isAdmin)
								{
						%>
						<li><a href="/todolistdao/controller/adminPage">Admin Page</a></li>
						<%
							}
						%>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/todolistdao/controller/home">Home</a></li>
				<li><a href="/todolistdao/controller/logout">Logout(<%=userName%>)
				</a></li>
			</ul>
		</div>
	</nav>


<br><br><br>

<fieldset>
<div align="center">
<h1>Welcome to the admin's page!</h1>
</div>
</fieldset>
<fieldset>
<div align="center">
<h1>Sessions Info</h1>
<h3>Created: <%=String.valueOf(application.getAttribute("createdSessionsCount")) == "null" ? 0:String.valueOf(application.getAttribute("createdSessionsCount"))%></h3>

<h3>Active: <%=(String.valueOf(application.getAttribute("activeSessionsCount")) == "null" ? 0 : String.valueOf(application.getAttribute("activeSessionsCount")))%></h3>

<h3>Destroyed: <%=String.valueOf(application.getAttribute("destroyedSessionsCount")) == "null" ? 0:String.valueOf(application.getAttribute("destroyedSessionsCount"))%></h3>



<br>
<br>
<br>
<h1>Attributes Info</h1>
<h3>Added: <%=String.valueOf(application.getAttribute("addedAttributesCount")) == "null"?0:String.valueOf(application.getAttribute("addedAttributesCount"))%></h3>

<h3>Active: <%=String.valueOf(application.getAttribute("activeAttributesCount")) == "null" ? 0:String.valueOf(application.getAttribute("activeAttributesCount"))%></h3>

<h3>Replaced: <%=String.valueOf(application.getAttribute("replacedAttributesCount")) =="null"?0:String.valueOf(application.getAttribute("replacedAttributesCount")) %></h3>

<h3>Removed: <%=String.valueOf(application.getAttribute("removedAttributesCount")) == "null"?0:String.valueOf(application.getAttribute("removedAttributesCount")) %></h3>
</div>
</fieldset>

</body>
</html>