<%--<jsp:useBean id="user" scope="request" type="il.ac.hit.todolistdao.model.User"/>
<jsp:setProperty name="user" property="*"/>
--%>
<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255" errorPage="errorPage.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css"  href="../css/button.css" />
<title>Register Page</title>
</head>
<body>
<%if(request.getAttribute("userMessage")!=null)
	{
		out.print(request.getAttribute("userMessage"));
	}
	%>
	<div align="center">
		<form action="/todolistdao/controller/register" method="get">
			<fieldset>
				Name: 
				<br />
				<input type="text" name="userName" />
				<br />
				Password:
				<br /> 
				<input type="password" name="password" />
				<br />
				<input type="checkbox" name="admin" value="true" > Set me as admin
				<br />
				<br />
				<input type="submit" class="mybtn" value="Register" />
			</fieldset>
		</form>
		<form action="todolistdao/controller/loginPage" method="get">
			<input type="submit" class="mybtn" value="Login" />
		</form>
	</div>
</body>
</html>


<%--
	il.ac.hit.todolistdao.model.User user = (il.ac.hit.todolistdao.model.User)(request.getAttribute("user"));
--%>