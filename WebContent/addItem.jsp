<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255" errorPage="errorPage.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/button.css" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1255">
<title>Add item page</title>
</head>
<body>

	<%
		String userName = (String) session.getAttribute("userName");
		boolean isAdmin = (session.getAttribute("admin")) == null ? false
				: ((boolean) session.getAttribute("admin"));
	%>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			<%
							if (isAdmin)
								{
						%>
						<li><a href="/todolistdao/controller/adminPage">Admin Page</a></li>
						<%
							}
						%>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/todolistdao/controller/home">Home</a></li>
				<li><a href="/todolistdao/controller/logout">Logout(<%=userName%>)
				</a></li>
			</ul>
		</div>
	</nav>



	<br>
	<br>
	<br>
	<h1 align="center">Add Item</h1>
	<br>
	<form action="/todolistdao/controller/addItem" method="get"
		class="form-horizontal" role="form">
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemTitle">Title:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemTitle"
					id="itemTitle" placeholder="Enter title">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemDescription">Description:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemDescription"
					id="itemDescription" placeholder="Enter description">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemDate">Date:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemDate" id="date"
					placeholder="dd-mm-yyyy">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="itemTime">Time:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="itemTime" id="date"
					placeholder="hh:mm:ss">
			</div>
		</div>
		<div class="form-group">
			<div align="center" class="col-sm-offset-1 col-sm-10">
				<button type="submit" class="btn btn-default">Add Item</button>
			</div>
		</div>
	</form>

</body>
</html>