<%@ page language="java" contentType="text/html; charset=windows-1255"
	pageEncoding="windows-1255" errorPage="errorPage.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/button.css" />
<link rel="stylesheet" type="text/css" href="./css/button.css" />
<title>Hibernate To Do List DAO</title>

</head>
<body>
	<h1 align="center" style="color: blue;">Welcome to your To Do List
		App!</h1>
	<div align="center">
		<form action="/todolistdao/controller/loginPage" method="get">
			<input type="submit" class="mybtn"
				value="Login" />
		</form>
		<br />
		<form action="/todolistdao/controller/registerPage" method="get">
			<input type="submit" class="mybtn"
				value="Register" />
		</form>
	</div>
</body>
</html>